import { Providers } from "../App";

import AddCustomerButton from "../Components/AddCustomerButton";
import CustomerAvatar from "../Components/CustomerAvatar";
import ViewCustomers from "../Components/ViewCustomers";

export default {
    title: "Customer Components",
    decorators: [(storyFn: () => JSX.Element) => <>{storyFn()}</>],
};

export const AddCustomerButtonStory = () => <Providers><AddCustomerButton /></Providers>
export const CustomerAvatarStory = () => <Providers><CustomerAvatar name='Aswin' /></Providers>
export const ViewCustomersStory = () => <Providers><ViewCustomers /></Providers>
