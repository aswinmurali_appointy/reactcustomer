import { Avatar } from "@mui/material";

const CustomerAvatar = ({ name }: { name: string }) => {
    return <Avatar color="red">{name[0]}</Avatar>
}

export default CustomerAvatar;
