import { Delete, Edit } from "@mui/icons-material";
import { List, ListItem, ListItemAvatar, ListItemButton, ListItemIcon, ListItemText } from "@mui/material";
import { useContext } from "react"
import CustomerContext, { Customer, CustomerContextType } from "../Models/Customer";
import CustomersContext, { CustomersContextType } from "../Models/Customers"
import RouteContext, { RouteContextType, Routes } from "../Models/Routes";
import CustomerAvatar from "./CustomerAvatar";

const ViewCustomers = () => {
  const { customers } = useContext(CustomersContext) as CustomersContextType;
  const { setCustomer } = useContext(CustomerContext) as CustomerContextType;
  const { setRoute } = useContext(RouteContext) as RouteContextType;

  const onDelete = (customer: Customer) => {
    setCustomer(customer)
    setRoute(Routes.deleteDialog);
  };

  const onUpdate = (customer: Customer) => {
    setCustomer(customer);
    setRoute(Routes.updateDialog);
  }

  const onDetails = (customer: Customer) => {
    setCustomer(customer);
    setRoute(Routes.detailsRoute);
  }

  return <nav aria-label="main mailbox folders">
    <List>{customers.map((customer) =>
      <ListItem>
        <ListItemButton >
          <ListItemAvatar>
            <CustomerAvatar name={customer.name} />
          </ListItemAvatar>
          <ListItemText onClick={() => onDetails(customer)}
            primary={customer.name} secondary={customer.email} />
          <ListItemIcon onClick={() => onDelete(customer)}>
            <Delete />
          </ListItemIcon>
          <ListItemIcon onClick={() => onUpdate(customer)} >
            <Edit />
          </ListItemIcon>
        </ListItemButton>
      </ListItem>
    )}</List>
  </nav>
}

export default ViewCustomers;
