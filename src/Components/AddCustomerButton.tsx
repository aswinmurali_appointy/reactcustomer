import { Add } from "@mui/icons-material";
import { Fab } from "@mui/material";
import { useContext } from "react";
import RouteContext, { RouteContextType, Routes } from "../Models/Routes";

const AddCustomerButton = () => {
    const { setRoute } = useContext(RouteContext) as RouteContextType;

    return <Fab color="primary" aria-label="add"
        onClick={() => setRoute(Routes.addDialog)}>
        <Add />
    </Fab> 
}

export default AddCustomerButton;
