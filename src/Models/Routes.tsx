import { createContext, useState } from "react"

export enum Routes {
    addDialog, updateDialog,
    deleteDialog, detailsRoute, none
}

export type RouteContextType = {
    route: Routes,
    setRoute: React.Dispatch<React.SetStateAction<Routes>>
}

export const RoutesProvider = ({ children }: { children: JSX.Element }) => {
    const [route, setRoute] = useState<Routes>(Routes.none);

    return <RouteContext.Provider value={{ route, setRoute }}>
        {children}
    </RouteContext.Provider>
}

const RouteContext = createContext<RouteContextType | null>(null);

export default RouteContext;
