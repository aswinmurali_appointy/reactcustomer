import { useState, createContext } from "react";
import { Customer } from "./Customer";

export type CustomersContextType = {
    customers: Customer[],
    addCustomer: (customer: Customer) => void,
    updateCustomer: (customer: Customer) => void,
    removeCustomer: (customer: Customer) => void,
    getCustomer: (name: string) => Customer,
}

const dummyCustomer = {
    name: 'Aswin Murali', email: 'aswin.m@appointy.com',
    phoneNo: '9203949505',
} as Customer;

export const CustomersProvider = ({ children }: { children: JSX.Element }) => {
    const [customers, setCustomers] = useState<Customer[]>([dummyCustomer]);

    const add = (customer: Customer) => {
        setCustomers([...customers, customer]);
    }

    const update = (customer: Customer) => {
        let loc = customers.findIndex((value) => value.email === customer.email);
        customers[loc] = customer;
        setCustomers([...customers]);
    }

    const remove = (customer: Customer) => {
        setCustomers(customers.filter((value) => value.email !== customer.email));
    }

    const get = (name: string) => {
        return customers.filter((value) => value.name === name)[0];
    }

    return <CustomersContext.Provider value={{
        customers,
        addCustomer: add,
        updateCustomer: update,
        removeCustomer: remove,
        getCustomer: get,
    }}>
        {children}
    </CustomersContext.Provider>
}

const CustomersContext = createContext<CustomersContextType | null>(null);

export default CustomersContext;
