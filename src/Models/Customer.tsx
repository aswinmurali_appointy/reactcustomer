import { useState, createContext } from "react";

export interface Customer {
    name: string, email: string, phoneNo: string
}

// Used to store the selected customer.

export type CustomerContextType = {
    customer: Customer,
    setCustomer: React.Dispatch<React.SetStateAction<Customer>>
}

export const CustomerProvider = ({ children }: { children: JSX.Element }) => {
    const [customer, setCustomer] = useState<Customer>({
        name: '', email: '', phoneNo: ''
    });

    return <CustomerContext.Provider value={{ customer, setCustomer }}>
        {children}
    </CustomerContext.Provider>
}

const CustomerContext = createContext<CustomerContextType | null>(null);

export default CustomerContext;
