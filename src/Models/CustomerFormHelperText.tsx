import { useState } from "react";

export const useFormHelperText = () => {
    const [nameHelper, setNameHelper] = useState('');
    const showNameHelper = () => setNameHelper('Please enter your name');

    const [emailHelper, setEmailHelper] = useState('');
    const showEmailHelper = () => setEmailHelper('Please enter valid email');

    const [phoneNoHelper, setPhoneNoHelper] = useState('');
    const showPhoneNoHelper = () => setPhoneNoHelper('Please enter valid phone number');

    const resetHelpers = () => {
        setNameHelper('');
        setEmailHelper('');
        setPhoneNoHelper('');
    }

    return {nameHelper, emailHelper, phoneNoHelper,
        showNameHelper, showEmailHelper, showPhoneNoHelper, resetHelpers} as const;
}
