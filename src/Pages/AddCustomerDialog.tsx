import { Close } from "@mui/icons-material"
import { Alert, Box, Button, Dialog, DialogActions, DialogContent, DialogTitle, IconButton, Snackbar, TextField } from "@mui/material"
import { useContext, useState } from "react"

import CustomerContext, { CustomerContextType } from "../Models/Customer";
import CustomersContext, { CustomersContextType } from "../Models/Customers";
import RouteContext, { RouteContextType, Routes } from "../Models/Routes";

import { useFormHelperText } from "../Models/CustomerFormHelperText";
import isEmail from "validator/lib/isEmail";
import isMobilePhone from "validator/lib/isMobilePhone";

export const AddCustomerDialog = () => {
    const [showNotification, setNotification] = useState(false);
    const { nameHelper, emailHelper, phoneNoHelper,
        showNameHelper, showEmailHelper, showPhoneNoHelper, resetHelpers } = useFormHelperText();

    const { route, setRoute } = useContext(RouteContext) as RouteContextType;
    const { customer, setCustomer }: CustomerContextType = useContext(CustomerContext) as CustomerContextType;
    const { addCustomer } = useContext(CustomersContext) as CustomersContextType;

    const onClose = () => setRoute(Routes.none);

    const onSave = () => {
        resetHelpers();
        if (!isEmail(customer.email)) {
            showEmailHelper();
        } else if (!isMobilePhone(customer.phoneNo)) {
            showPhoneNoHelper();
        } else if (customer.name === '' || customer.name === ' ') {
            showNameHelper();
        }
        else {
            addCustomer(customer);
            setNotification(true);
            onClose();
        }
    }

    return <>
        <Snackbar open={showNotification} anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center'
        }} onClose={() => setNotification(false)} autoHideDuration={2000} >
            <Alert severity='success' onClose={() => setNotification(false)} >
                Customer added successfully
            </Alert>
        </Snackbar>
        <Dialog onClose={onClose} open={route === Routes.addDialog} fullWidth={true}>
            <DialogTitle>
                <Box display='flex' alignItems='center'>
                    <Box flexGrow={1}>New Customer</Box>
                    <Box>
                        <IconButton onClick={onClose}><Close /></IconButton>
                    </Box>
                </Box>
            </DialogTitle>
            <DialogContent>
                <TextField
                    fullWidth={true} label='Name' variant='standard' required helperText={nameHelper}
                    onChange={(value) => setCustomer({ ...customer, name: value.target.value })}
                />
            </DialogContent>
            <DialogContent>
                <TextField
                    fullWidth={true} label='Email' variant='standard' required helperText={emailHelper}
                    onChange={(value) => setCustomer({ ...customer, email: value.target.value })}
                />
            </DialogContent>
            <DialogContent>
                <TextField
                    fullWidth={true} label='Phone' variant='standard' required helperText={phoneNoHelper}
                    onChange={(value) => setCustomer({ ...customer, phoneNo: value.target.value })}
                />
            </DialogContent>
            <DialogActions>
                <Button onClick={onClose}>CANCEL</Button>
                <Button onClick={onSave} variant='contained'>SAVE</Button>
            </DialogActions>
        </Dialog>
    </>
}

export default AddCustomerDialog;
