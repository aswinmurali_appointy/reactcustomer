import { Card, CardContent, Container, Drawer, Grid, List, ListItem, ListItemAvatar, ListItemButton, ListItemText, MenuItem, Select, Typography } from "@mui/material";
import { useContext } from "react";
import CustomerAvatar from "../Components/CustomerAvatar";
import CustomerContext, { CustomerContextType } from "../Models/Customer";
import RouteContext, { RouteContextType, Routes } from "../Models/Routes";

const DetailsCustomerDrawer = () => {
    const { route, setRoute } = useContext(RouteContext) as RouteContextType;
    const { customer } = useContext(CustomerContext) as CustomerContextType;

    const onClose = () => setRoute(Routes.none);

    return <Drawer open={route === Routes.detailsRoute}
        anchor={'right'} onClose={onClose} PaperProps={{ sx: { width: "60%" } }}>
        <List>
            <ListItem>
                <ListItemButton >
                    <ListItemAvatar>
                        <CustomerAvatar name={customer.name} />
                    </ListItemAvatar>
                    <ListItemText primary={customer.name}
                        secondary={customer.email} />
                </ListItemButton>
            </ListItem>
        </List>
        <Container>
            <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
                DETAILS
            </Typography>
            <Card>
                <CardContent>
                    <Typography sx={{ mb: 1.5 }} color="text.secondary">
                        Company
                    </Typography>
                    <Typography sx={{ mb: 1.5 }} color="text.secondary">
                        Address
                    </Typography>
                    <Typography sx={{ mb: 1.5 }} color="text.secondary">
                        Telephones
                    </Typography>

                    <Grid container spacing={4} style={{
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center"
                    }}>
                        <Grid item xs={4}>
                            <Typography sx={{ mb: 1.5 }} color="text.secondary">
                                Language
                            </Typography>
                        </Grid>
                        <Grid item xs={8}>
                            <Select value={"English"}>
                                <MenuItem value={'English'}>English</MenuItem>
                            </Select>
                        </Grid>
                    </Grid>

                    <Grid container spacing={4} style={{
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                        paddingTop: '10px'
                    }}>
                        <Grid item xs={4}>
                            <Typography sx={{ mb: 1.5 }} color="text.secondary">
                                Timezone
                            </Typography>
                        </Grid>
                        <Grid item xs={8}>
                            <Select value={'Asia/Kolkata'}>
                                <MenuItem value={'Asia/Kolkata'}>Asia/Kolkata</MenuItem>
                            </Select>
                        </Grid>
                    </Grid>
                </CardContent>
            </Card>
            <Typography sx={{ fontSize: 14, paddingTop: '20px' }} color="text.secondary" gutterBottom>
                SPECIAL DATES
            </Typography>
            <Card>
                <CardContent>
                    <Typography color="text.secondary">
                        Birth Dates
                    </Typography>
                </CardContent>
            </Card>
        </Container>
    </Drawer >
}

export default DetailsCustomerDrawer;
