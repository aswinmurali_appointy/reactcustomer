import { Box, Grid, Typography, TextField } from "@mui/material"
import AddCustomerButton from "../Components/AddCustomerButton"
import ViewCustomers from "../Components/ViewCustomers"

const MainPage = () => (
    <Box sx={{ p: 3 }} >
        <Grid justifyContent="flex-end" alignContent={'space-between'} container spacing={2}>
            <Grid item xs={7}>
                <Typography variant="h3">
                    Customers
                </Typography>
            </Grid>
            <Grid item xs={4}>
                <Box display="flex" justifyContent="flex-end">
                    <TextField label="Type to search..." fullWidth variant="outlined" />
                </Box>
            </Grid>
            <Grid item xs={1}>
                <Box display="flex" justifyContent="flex-end">
                    <AddCustomerButton />
                </Box>
            </Grid>
            <Grid item xs={12} >
                <ViewCustomers />
            </Grid>
        </Grid>
    </Box>
)

export default MainPage;
