import { Snackbar, Alert, Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, Button } from "@mui/material";
import { useContext, useState } from "react";
import CustomerContext, { CustomerContextType } from "../Models/Customer";
import CustomersContext, { CustomersContextType } from "../Models/Customers";
import RouteContext, { Routes, RouteContextType } from "../Models/Routes";

const DeleteCustomerDialog = () => {
    const [showNotification, setNotification] = useState(false);

    const { route, setRoute } = useContext(RouteContext) as RouteContextType;
    const { customer } = useContext(CustomerContext) as CustomerContextType;
    const { removeCustomer } = useContext(CustomersContext) as CustomersContextType;

    const onClose = () => setRoute(Routes.none);

    const onConfirm = () => {
        removeCustomer(customer);
        setNotification(true);
        onClose();
    }

    return <>
        <Snackbar open={showNotification} anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center'
        }} onClose={() => setNotification(false)} autoHideDuration={2000} >
            <Alert severity='info' onClose={() => setNotification(false)} >
                Customer deleted successfully
            </Alert>
        </Snackbar>
        <Dialog onClose={onClose} open={route === Routes.deleteDialog}>
            <DialogTitle>Delete Customer</DialogTitle>
            <DialogContent>
                <DialogContentText id='alert-dialog-description'>
                    Are you sure you want to continue?
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button onClick={onClose}>CANCEL</Button>
                <Button onClick={onConfirm} variant='contained'>CONFIRM</Button>
            </DialogActions>
        </Dialog>
    </>
}

export default DeleteCustomerDialog;
