import { CustomersProvider } from "./Models/Customers";
import { RoutesProvider } from "./Models/Routes";
import { AddCustomerDialog } from "./Pages/AddCustomerDialog";
import { CustomerProvider } from "./Models/Customer";
import DeleteCustomerDialog from "./Pages/DeleteCustomerDialog";
import UpdateCustomerDialog from "./Pages/UpdateCustomerDialog";
import MainPage from "./Pages/MainPage";
import DetailsCustomerDrawer from "./Pages/DetailsCustomerDrawer";

export const Providers = ({ children }: { children: JSX.Element }) => (
  <RoutesProvider>
    <CustomerProvider>
      <CustomersProvider>
        {children}
      </CustomersProvider>
    </CustomerProvider>
  </RoutesProvider>
)

const App = () => <Providers>
  <>
    {/* Dialogs */}
    <AddCustomerDialog />
    <DeleteCustomerDialog />
    <UpdateCustomerDialog />
    <DetailsCustomerDrawer />

    {/* Pages */}
    <MainPage />
  </>
</Providers>

export default App;
